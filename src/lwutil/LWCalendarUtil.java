package lwutil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Mitanjo
 */
public class LWCalendarUtil {
    private static LWCalendarUtil instance = new LWCalendarUtil();
    
    private LWCalendarUtil() {}
    
    public static LWCalendarUtil getInstance() {
        return instance;
    }

    private static final String LANG_FR = "FR";
    private static final String LANG_ENG = "ENG";
    private static final String UNDEFINED_LANG = "Undefined language";
    private String currentLang = LANG_FR;

    public static final int DD_MM_YYYY = 1;
    public static final int DD_M_YYYY = 2;
    public static final int YYYY_MM_DD = 3;
    
    private static final String[] months_fr = {"Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin",
        "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"};

    private static final String[] months2_fr = {"jan", "fev", "mars", "avr", "mai", "juin",
        "juil", "aout", "sept", "oct", "nov", "dec"};

    private static final String[] weeks_fr = {"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi",
        "Samedi", "Dimanche"};

    private static final String[] weeks2_fr = {"Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"};
    
    private static final String[] months_eng = {"January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "Novembre", "December"};
    
    private static final String[] months2_eng = {"jan", "feb", "mar", "may", "jun",
        "jul", "aug", "sep", "oct", "nov", "dec"};
    
    private static final String[] weeks_eng = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
        "Saturday", "Sunday"};

    private static final String[] weeks2_eng = {"Mon", "Tue", "Wed", "Thu", "Fri",
        "Sat", "Sun"};
    
    public void setCurrentLang(String lang) {
        currentLang = lang;
    }
    
    public String getMonthName(int m) {
        if(currentLang.equals(LANG_FR))
            return months_fr[m];
        else if(currentLang.equals(LANG_ENG))
            return months_eng[m];
        
        return UNDEFINED_LANG;
    }
    
    public String getMonthSimpleName(int m) {
        if(currentLang.equals(LANG_FR))
            return months2_fr[m];
        else if(currentLang.equals(LANG_ENG))
            return months2_eng[m];
        
        return UNDEFINED_LANG;
    }
    
    public String getDayOfWeek(int d) {
        if(currentLang.equals(LANG_FR))
            return weeks_fr[d];
        else if(currentLang.equals(LANG_ENG))
            return weeks_eng[d];
        
        return UNDEFINED_LANG;
    }

    public String getSimpleDayOfWeek(int d) {
        if(currentLang.equals(LANG_FR))
            return weeks2_fr[d];
        else if(currentLang.equals(LANG_ENG))
            return weeks2_eng[d];

        return UNDEFINED_LANG;
    }

    /**
     * Permet de savoir si une année est bissextile ou non
     * @param annee
     * @return true si l'année en question est bissextile sinon retourne false
     */
    public boolean isBissextile(int annee) {
        if(annee%4 == 0)
            if(annee%100 != 0) return true;
            else if(annee%400 == 0) return true;
        return false;
    }
    
    /**
     * Obtenir le jour maximal d'un mois de l'année
     * @param mois
     * Mois de l'année, le premier indice est 0
     * @param annee
     * L'année en question
     * @return
     */
    public int obtenirJmax(int mois, int annee) {
        int m = mois;
        if(m == 0 || m == 2 || m == 4 || m == 6 || m == 7 || m == 9 || m == 11)
            return 31;
        else if(m == 1) {
            if(isBissextile(annee)) return 29;
            else return 28;
        } else return 30;
    }
    
    /**
     * Obtenir le jour de la semaine, le premier indice commence par 0
     * @param j
     * @param m
     * Le premier mois commence par 0
     * @param a
     * @return
     */
    public int obtenirIndiceSem(int j, int m, int a) {
        int somme = j;

        for(int i = 0; i < m; i++)
            somme += obtenirJmax(i, a);
        somme += a + 4 + (a - 1) / 4;

        return (somme%7);
    }
    
     /**
     * Obtenir la date du lendemain. Le mois commence par l'indice 0
     * @param date
     * @return
     */
     public Date obtenirJourSuivant(Date date) {
        int jour, mois, annee;
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        jour = cld.get(Calendar.DAY_OF_MONTH);
        mois = cld.get(Calendar.MONTH);
        annee = cld.get(Calendar.YEAR);

        if(jour == cld.getMaximum(Calendar.DAY_OF_MONTH)) {
            jour = 1;
            if(mois == 11) {
                mois = 0;
                annee++;
            }else
                mois++;
        }
        else
        {
            jour++;
        }

        cld.set(annee, mois, jour);
        return cld.getTime();
    }
     
      /**
     * Compte le nombre de jours restant avant la fin de la semaine
     * Si la date spécifié est un Lundi, alors il renvoie 7, si la
     * date spécifié est un Dimanche alors, il renvoie 0, ... etc
     * @param j
     * @param m
     * @param a
     * @return
     * Nombre de jours restants avant la fin de semaine
     */
    public int nbJoursRestant(int j, int m, int a) {
        int iSem;

        Calendar cld = Calendar.getInstance();
        cld.set(a, m, j);
        iSem = cld.get(Calendar.DAY_OF_WEEK);

        return (7 - iSem);
    }
    
     /**
     * Définir une date avec le jour, le mois et l'année
     * @param j
     * Jour de l'année
     * @param m
     * Mois de l'année
     * @param a
     * Année
     * @return
     * Instance de la classe java.util.Date
     */
    public Date creerDate(int j, int m, int a) {
        Calendar cld = Calendar.getInstance();
        cld.set(a, m-1, j, 0, 0, 0);
        Date laDate = cld.getTime();
        return laDate;
    }

    /**
     * Créer une date avec le jour, le mois l'année et l'heure
     * @param j
     * Jour de l'année
     * @param m
     * Mois de l'année
     * @param a
     * Année
     * @param h
     * Heure, de 0 à 23
     * @param mn
     * Minute
     * @param s
     * Seconde
     * @return
     */
    public Date creerDate(int j, int m, int a, int h, int mn, int s) {
        Calendar cld = Calendar.getInstance();
        cld.set(a, m-1, j, h, mn, s);
        Date laDate = cld.getTime();
        return laDate;
    }

    /**
     * Vérifie si la date spécifiée est valide
     * @param j
     * Jour de l'année
     * @param m
     * Mois de l'année
     * @param a
     * Année
     * @return
     * true si la date est valide, false dans le cas echéant
     */
    public boolean dateValide(int j, int m, int a) {
        Calendar cld = Calendar.getInstance();
        cld.setLenient(false);
        cld.set(a, m, j);

        try {
            cld.getTime();

        } catch(IllegalArgumentException e) {
            return false;
        }

        return true;
    }

    /**
     * Convertir une chaîne en date
     * @param date
     * Chaîne au format 'jj-mm-yyyy'
     * @return
     * Instance de la classe java.util.Date
     */
    public Date stringToDate(String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date maDate = dateFormat.parse(date);
            return maDate;

        } catch(ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Date stringToDate(String date, String separator) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd" + separator + "MM" + separator + "yyyy");
            Date maDate = dateFormat.parse(date);
            return maDate;

        } catch(ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Convertir une date en chaîne
     * @param date
     * Instance de la classe java.util.Date
     * @return
     * Châine au format 'jj-mm-yyyy'
     */
    public String dateToString(Date date) {
        if(date == null) return "";

        Calendar cld = Calendar.getInstance();
        cld.setTime(date);

        int annee = cld.get(Calendar.YEAR);
        // Le mois du système commence par 0 et non par 1
        int mois = cld.get(Calendar.MONTH) + 1;
        int jour = cld.get(Calendar.DAY_OF_MONTH);

        String str1, str2;

        if(mois < 10) str1 = "0" + mois;
        else str1 = "" + mois;
        if(jour < 10) str2 = "0" + jour;
        else str2 = "" + jour;

        String str = "" + str2 + "-" + str1 + "-"  + annee;

        return str;
    }

    public String dateToString(Date date, int dateFormat) {
        if(date == null) return "";

        Calendar cld = Calendar.getInstance();
        cld.setTime(date);

        int annee = cld.get(Calendar.YEAR);
        // Le mois du système commence par 0 et non par 1
        int mois = cld.get(Calendar.MONTH) + 1;
        int jour = cld.get(Calendar.DAY_OF_MONTH);

        String str1, str2;

        if(mois < 10) str1 = "0" + mois;
        else str1 = "" + mois;
        if(jour < 10) str2 = "0" + jour;
        else str2 = "" + jour;

        String str = "";

        switch(dateFormat) {
            case DD_MM_YYYY: {
                str += "" + str2 + "/" + str1 + "/"  + annee;
            }break;
            case DD_M_YYYY: {
                str += "" + str2 + "/" + cld.get(Calendar.MONTH) + "/"  + annee;
            }break;
            case YYYY_MM_DD: {
                str += "" + annee + "/" + str1 + "/"  + str2;
            }break;
        }        

        return str;
    }

    public String dateToString(Date date, String separator) {
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);

        int annee = cld.get(Calendar.YEAR);
        // Le mois du système commence par 0 et non par 1
        int mois = cld.get(Calendar.MONTH) + 1;
        int jour = cld.get(Calendar.DAY_OF_MONTH);

        String str1, str2;

        if(mois < 10) str1 = "0" + mois;
        else str1 = "" + mois;
        if(jour < 10) str2 = "0" + jour;
        else str2 = "" + jour;

        String str = "" + str2 + separator + str1 + separator  + annee;

        return str;
    }

    /**
     * Calcule le nombre de jours entre deux dates
     * @param debut
     * Date de début
     * @param fin
     * Date de fin
     * @return
     * Retourne un entier négatif si la date de fin est antérieur
     * à la date de début, retourne un entier positif dans le cas contraire
     */
    public int nbJours(Date debut, Date fin) {
        long res = 0;

        res = (fin.getTime() - debut.getTime()) / 1000 / 60 / 60 / 24;

        return (int)res;
    }

    /**
     * Solution correction de bug 09/12/08
     * Calcule la valeur algébrique qui sépare deux dates. Pour eviter des résulats
     * innatendus, vérifier bien l'heure associée à la date
     * @param debut
     * Date de début
     * @param fin
     * Date de fin
     * @param ech
     * Echelle de la valeur algébrique
     * @return
     * Valeur en entier
     */
    public int nbJours(Date debut, Date fin, int ech) {
        long res = 0;
 
        res = (fin.getTime() - debut.getTime()) * ech / 1000 / 60 / 60 / 24;

        return (int)res;
    }
}
