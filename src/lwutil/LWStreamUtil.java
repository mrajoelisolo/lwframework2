package lwutil;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author Mitanjo
 */
public class LWStreamUtil {
    private static LWStreamUtil instance = new LWStreamUtil();

    private LWStreamUtil() {}

    public static LWStreamUtil getInstance() {
        return instance;
    }

    public byte[] toBytes(String path) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] res = null;

        try{
            FileInputStream fis = new FileInputStream(path);
            int count = 0;
            while(fis.available() > 0) {
                baos.write(fis.read());
                count++;
            }
            fis.close();

            res = baos.toByteArray();
            baos.flush();
        } catch(IOException e) {
            e.printStackTrace();
        }

        return res;
    }
}
