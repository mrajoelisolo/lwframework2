package lwutil;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 *
 * @author Mitanjo
 */
public class LWTreeUtil {
    private static LWTreeUtil instance = new LWTreeUtil();
    
    private LWTreeUtil() {}
    
    public static LWTreeUtil getInstance() {
        return instance;
    }
    
    /**
     * Déploie toutes les noeuds de l'arbre
     * @param arbre
     * Instance d'un composant JTree
     */
    public void deployTree(JTree tree) {
        int n = tree.getRowCount();
        int i = 0;
        while(i < n) {
            TreePath tp = tree.getPathForRow(i);
            tree.expandPath(tp);
            n = tree.getRowCount();
            i++;
        }
    }
    
    /**
     * @since 29/12/08
     * Parcourir l'arbre d'une instance d'un composant JTreeTable
     * Note : Seules les noeuds visibles sont dans la liste
     * @param arbre
     * Instance du composant JTreeTable
     * @return 
     * Liste des elements de l'arbre parcourue
     */
    public List browseJTree(JTree tree) {
        int n = tree.getRowCount();
        List res = null;
        
        if(n > 0) res = new ArrayList();
        
        for(int i = 0; i < n; i++) {
            TreePath tp = tree.getPathForRow(i);
            DefaultMutableTreeNode noeud = (DefaultMutableTreeNode) tp.getLastPathComponent();
            res.add(noeud);
        }
        
        return res;
    }
    
    private List<DefaultMutableTreeNode> nodes = new ArrayList();
    /**
     * Parcours l'ensemble des noeuds a partir de la racine
     * Le parcours des noeuds se fait de haut en bas
     * @param r
     * La racine de tous les noeuds
     * @return
     * Liste des noeuds
     */
    public List getListFromNode(DefaultMutableTreeNode r) {
        nodes.clear();
        nodes.add(r);
        browseChilds(r);
        return nodes;
    }
    private  void browseChilds(DefaultMutableTreeNode r) {
        Enumeration n = r.children();        
        while(n.hasMoreElements()) {
            DefaultMutableTreeNode e = (DefaultMutableTreeNode) n.nextElement();
            nodes.add(e);
            browseChilds(e);
        }
    }
}
