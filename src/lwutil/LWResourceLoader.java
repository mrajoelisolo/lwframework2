package lwutil;

import java.awt.Image;
import java.io.File;
import java.net.URI;
import java.net.URL;
import javax.swing.Icon;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class LWResourceLoader {
	private static LWResourceLoader instance = new LWResourceLoader();

	private LWResourceLoader() {}

	public static LWResourceLoader getInstance() {
		return instance;
	}

	public Image loadImage(String path) {
		Image res = null;

		try {
			URL url = ClassLoader.getSystemClassLoader().getResource(path);
			ImageIcon icon = new ImageIcon(url);
			res = icon.getImage();
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Error on : " + this.getClass().getSimpleName() + " : loadImage() : " + e.getMessage());
		}

		return res;
	}

        public Icon loadIcon(String path) {
            Icon res = null;

		try {
			URL url = ClassLoader.getSystemClassLoader().getResource(path);
			res = new ImageIcon(url);
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Error on : " + this.getClass().getSimpleName() + " : loadImage()");
		}

		return res;
        }

        public File loadFile(String path) {
            File res = null;

            try {
                URI uri = ClassLoader.getSystemClassLoader().getResource(path).toURI();
		res = new File(uri);
            }catch(Exception e) {
		JOptionPane.showMessageDialog(null, "Error : " + this.getClass().getSimpleName() + " : loadFile()");
            }

            return res;
        }
}
