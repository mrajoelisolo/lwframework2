package lwutil;

import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Mitanjo
 */
public class LWKeyUtilities {
    private static LWKeyUtilities instance = new LWKeyUtilities();
    private int[] neutralKeys = {16, 17, 18, 19, 20, 27, 33, 34, 35, 36, 37, 38, 39, 40, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 127, 154, 155, 524, 525};
    private Set set = new HashSet();

    private LWKeyUtilities() {
        for(int i : neutralKeys)
            set.add(i);
    }

    public static LWKeyUtilities getInstance() {
        return instance;
    }

    public boolean isModifierKey(KeyEvent e) {
        return !set.contains(e.getKeyCode());
    }

    public boolean isNeutralKey(KeyEvent e) {
        return set.contains(e.getKeyCode());
    }
}
