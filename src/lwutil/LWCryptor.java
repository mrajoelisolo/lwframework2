package lwutil;

import javax.swing.JOptionPane;

/**
 *
 * @author Mitanjo
 */
public class LWCryptor {
    private static LWCryptor instance = new LWCryptor();

    private LWCryptor() {}

    public static LWCryptor getInstance() {
        return instance;
    }

    private String key;

    public void initialize(String key) {
        this.key = key;
    }

    private boolean check() {
        if(key == null) {
            JOptionPane.showMessageDialog(null, "The key is not inizialized.", "LWCryptor", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        return true;
    }

    public String encrypt(String str) {
        if(check()) return "";

        String res = "";

        int i = 0;
        int j = 0;
        while(i < str.length()) {
            //maka caractère iray ao amin'ny ilay clé
            char c = key.charAt(j);
            //maka caractère iray ao amin'ny ilay message claire
            char s = str.charAt(i);

            //fonction f
            res += getSplittedChar(s, c);

            i++;
            j++;

            if(j >= key.length()) j = 0;
        }

        return res;
    }

    public String decrypt(String str) {
        if(check()) return "";

        String res = "";

        int i = 0;
        int j = 0;
        while(i < str.length()) {
           //maka caractère iray ao amin'ny ilay clé
            char c = key.charAt(j);
            //maka caractère iray ao amin'ny ilay message claire
            char s = str.charAt(i);

            res += getSplittedChar(s, -c);

            i++;
            j++;

            if(j >= key.length()) j = 0;
        }

        return res;
    }

    private char getSplittedChar(char c, int key) {
        int vKey = key%256;
        char res = getCharAt(c + vKey);

        if(getCharAt(c + vKey) > 255) {
            return getCharAt(c + vKey - 255 - 1);
        }else if(getCharAt(c + vKey) < 0) {
            return getCharAt(255 - (Math.abs(vKey) - c) + 1);
        }

        return res;
    }

    public char getCharAt(int i) {
        char res = '\0';

        try {
            res = String.format("%c", i).charAt(0);
        }catch(Exception e) {
            e.printStackTrace();
        }

        return res;
    }
}
