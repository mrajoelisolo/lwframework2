package lwutil;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.swing.RepaintManager;

/**
 *
 * @author FireWolf
 */
public class LWPrintManager implements Printable {
    private Component[] components;
    private static int wdt = 2000, hgt = 2000;
    private static int orientation;
    
    public LWPrintManager(Component[] components) {
        this.components = components;
    }
    
    public static void printComponent(Component[] c, int orientation, String typePapier) {
        int tmp;
        
        LWPrintManager.orientation = orientation;
        if(typePapier.equals("A5")) {
            tmp = wdt;
            wdt = hgt / 2;
            hgt = tmp;
        }
        if(orientation == PageFormat.LANDSCAPE) {
            tmp = wdt;
            wdt = hgt;
            hgt = tmp;
        }
        
        new LWPrintManager(c).print();
    }
   
    public void print() {
        PrinterJob printJob = PrinterJob.getPrinterJob();
        PageFormat pageFormat = new PageFormat();
        Paper paper = new Paper();
        paper.setImageableArea(0, 0, wdt, hgt);
        pageFormat.setPaper(paper);
        pageFormat.setOrientation(orientation);
        printJob.setPrintable(this, pageFormat);
        printJob.setCopies(1);
        
        if(printJob.printDialog()) {
            try{
                printJob.print();
            }catch(PrinterException e) {
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public int print(Graphics g, PageFormat pageFormat, int pageIndex) {
        if(pageIndex > (components.length - 1 )) {
            return NO_SUCH_PAGE;
        } else {
            Graphics2D g2 = (Graphics2D) g;
            
            g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
            disableDoubleBuffering(components[pageIndex]);
            components[pageIndex].paint(g2);
            enableDoubleBuffering(components[pageIndex]);
            return PAGE_EXISTS;
        }
    }

    private void disableDoubleBuffering(Component component) {
        RepaintManager currentManager = RepaintManager.currentManager(component);
        currentManager.setDoubleBufferingEnabled(false);
    }

    private void enableDoubleBuffering(Component component) {
        RepaintManager currentManager = RepaintManager.currentManager(component);
        currentManager.setDoubleBufferingEnabled(true);
    }
}
