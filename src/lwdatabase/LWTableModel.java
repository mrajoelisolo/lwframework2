package lwdatabase;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Administrateur
 */
public class LWTableModel extends AbstractTableModel {

    private Object[][] donnees;
    private String[] titres;

    public LWTableModel(Object donnees[][], String titres[]) {
        this.donnees = donnees;
        this.titres = titres;
    }

    public int getColumnCount() {
        return donnees[0].length;
    }

    public Object getValueAt(int row, int col) {
        return donnees[row][col];
    }

    public int getRowCount() {
        return donnees.length;
    }

    //New : permet entre autres d'afficher les images
    @Override
    public Class getColumnClass(int c) {
        if(getValueAt(0, c) == null) return Object.class;
        return getValueAt(0, c).getClass();
    }

    @Override
    public String getColumnName(int col) {
        return titres[col];
    }
}

