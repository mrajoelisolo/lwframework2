package lwdatabase.filter;

/**
 *
 * @author jarod
 */
public enum EnumFilter {
    EQUAL, BETWEEN, INFERIOR_OR_EQUAL, SUPERIOR_OR_EQUAL
}
