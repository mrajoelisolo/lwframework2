package lwdatabase.filter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mitanjo
 */
public class QueryBetweenExpression implements IQueryExpression {
    private String field;
    private Object value1;
    private Object value2;

    public QueryBetweenExpression(String field, Object value1, Object value2) {
        this.field = field;
        this.value1 = value1;
        this.value2 = value2;
    }

    @Override
    public String getExpression() {
        return " " + field + " between :" + field + "1 and :" + field + "2 ";
    }

    @Override
    public List<LWParameter> getParameters() {
        List<LWParameter> res = new ArrayList();

        res.add(new LWParameter(field + "1", value1));
        res.add(new LWParameter(field + "2", value2));

        return res;
    }
}
