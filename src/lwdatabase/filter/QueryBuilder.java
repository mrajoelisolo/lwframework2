package lwdatabase.filter;

/**
 *
 * @author Mitanjo
 */
public class QueryBuilder {
    private static QueryBuilder instance = new QueryBuilder();

    private QueryBuilder() {}

    public static QueryBuilder getInstance() {
        return instance;
    }

    public IQueryExpression getExpression(EnumFilter comparator, String field, Object value1, Object value2) {
        IQueryExpression res = null;

        switch(comparator) {
            case EQUAL : {
                res = new QueryEqualExpression(field, value1);
            };break;

            case INFERIOR_OR_EQUAL : {
                res = new QueryInferiorOrEqualExpression(field, value1);
            };break;

            case SUPERIOR_OR_EQUAL : {
                res = new SuperiorOrEqualExpression(field, value1);
            };break;

            case BETWEEN : {
                res = new QueryBetweenExpression(field, value1, value2);
            };
        }
        return res;
    }
}
