package lwdatabase.filter;

import java.util.List;

/**
 *
 * @author Mitanjo
 */
public interface IQueryExpression {
    public String getExpression();
    public List<LWParameter> getParameters();
}
