package lwdatabase.filter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mitanjo
 */
public class QueryEqualExpression implements IQueryExpression {
    private String field;
    private Object value;

    public QueryEqualExpression(String field, Object value) {
        this.field = field;
        this.value = value;
    }
    
    @Override
    public String getExpression() {
        if(value instanceof String)
            return " " + field + " like :" + field + " ";
        else
            return " " + field + " = :" + field + " ";
    }

    @Override
    public List<LWParameter> getParameters() {
        List<LWParameter> res = new ArrayList();

        if(value instanceof String)
            res.add(new LWParameter(field, "%" + value + "%"));
        else
            res.add(new LWParameter(field, value));

        return res;
    }
}
