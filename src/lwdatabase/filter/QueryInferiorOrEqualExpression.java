package lwdatabase.filter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mitanjo
 */
public class QueryInferiorOrEqualExpression implements IQueryExpression {
    private String field;
    private Object value;

    public QueryInferiorOrEqualExpression(String field, Object value) {
        this.field = field;
    }

    @Override
    public String getExpression() {
        return " " + field + " <= :" + field + " ";
    }

    @Override
    public List<LWParameter> getParameters() {
        List<LWParameter> res = new ArrayList();

        res.add(new LWParameter(field, value));

        return res;
    }

//    public LWParameter[] getParameters() {
//        LWParameter[] res = new LWParameter[1];
//
//        res[0] = new LWParameter(field, value);
//
//        return res;
//    }


}
