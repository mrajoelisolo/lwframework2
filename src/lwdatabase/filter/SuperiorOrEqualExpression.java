package lwdatabase.filter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mitanjo
 */
public class SuperiorOrEqualExpression implements IQueryExpression {
    private String field;
    private Object value;

    public SuperiorOrEqualExpression(String field, Object value) {
        this.field = field;
        this.value = value;
    }

    @Override
    public String getExpression() {
        return " " + field + " >= :" + field + " ";
    }

    @Override
    public List<LWParameter> getParameters() {
        List<LWParameter> res = new ArrayList();

        res.add(new LWParameter(field, value));

        return res;
    }

//    @Override
//    public LWParameter[] getParameters() {
//        LWParameter[] res = new LWParameter[1];
//
//        res[0] = new LWParameter(field, value);
//
//        return res;
//    }
}
