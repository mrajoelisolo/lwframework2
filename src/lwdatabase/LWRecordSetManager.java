package lwdatabase;

import javax.swing.JTable;
import lwdatabase.LWRecordSet;

/**
 *
 * @author Mitanjo
 */
public class LWRecordSetManager {
    private static LWRecordSetManager instance = new LWRecordSetManager();
    
    private LWRecordSetManager() {}
    
    public static LWRecordSetManager getInstance() {
        return instance;
    }
    
    private LWRecordSet recordSet;
    private int currentIndex = 0;
    
    public void setCurentRecordSet(LWRecordSet recordSet) {
        this.recordSet = recordSet;
    }
    
    public LWRecordSet getCurrentRecordSet() {
        return recordSet;
    }
    
    public void setCurrentIndex(JTable table) {
        int i = table.getSelectedRow();

        table.setRowSelectionInterval(i, i);
        currentIndex = i;
    }
    
    public int getCurrentIndex() {
        return currentIndex;
    }
}
