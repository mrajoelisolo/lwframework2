package lwdatabase;

/**
 *
 * @author jarod
 */
public class EColumnModel {
    public static String STRING_TYPE = "String";
    public static String DATE_TYPE = "Date";
    public static String FLOAT_TYPE = "Float";
    public static String INTEGER_TYPE = "Integer";
    public static String IMAGE_TYPE = "Image";

    private String text;
    private String name;
    private String type;
    private EnumDateFormat dateFormat = EnumDateFormat.DD_MM_YYYY;

    public EColumnModel(String text, String name) {
        this.name = name;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setDateFormat(EnumDateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    public EnumDateFormat getDateFormat() {
        return dateFormat;
    }
}
