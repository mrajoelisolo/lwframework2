package lwdatabase;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author RAJOELISOLO Mitanjo
 * L'utilité de cette classe est équivalent à un RecordSet de VB, elle
 * permet de parcourir la liste et de faire diverses opérations
 */
public class LWRecordSet {
    private List rows = new ArrayList();
    
    public LWRecordSet() {}
    
    public LWRecordSet(Object[] value) {
        setRows(value);
    }
    
    public Object getValueAt(int n) {
        return rows.get(n);
    }
    
    public void setRows(Object[] value) {
        rows.clear();

        for(Object o : value)
            this.rows.add(o);
    }
    
    public int size() {
        return rows.size();
    }

    public void addRow(Object o) {
        rows.add(o);
    }

    public void remove(Object o) {
        rows.remove(o);
    }

    public List rows() {
        return rows;
    }

    public void setValueAt(int i, Object value) {
        rows.set(i, value);
    }
    
    public int getIndexOf(Object o) {
        int res = rows.indexOf(o);
        if(res != -1) return res;
        return 0;
    }

    public void clear() {
        rows.clear();
    }
}
