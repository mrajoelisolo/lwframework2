package lwdatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JTable;
import lwutil.LWCalendarUtil;

/**
 *
 * @author Mitanjo
 */
public class LWTableLoader {
    private static LWTableLoader instance = new LWTableLoader();

    private LWTableLoader() {}

    public static LWTableLoader getInstance() {
        return instance;
    }

    public int fillTable(JTable table, EntityModel em, List<IEntity> data) {
        List<IEntity> szData = data;

        if(szData == null)
            szData = new ArrayList<IEntity>();

        int colCount = em.getColumnsModel().size();
        String[] headers = new String[colCount];
        int rows = szData.size();

        for(int i = 0; i < colCount; i++)
            headers[i] = em.getColumnsModel().get(i).getText();

        Object[][] rSet = new Object[rows][colCount];

        int i = 0;
        for(Object o : szData) {
            int j = 0;

            for(EColumnModel ecm : em.getColumnsModel()) {
                try {
                    if(ecm.getType().equals(EColumnModel.DATE_TYPE)) {
                        String d = "";

                        if(ecm.getDateFormat().equals(EnumDateFormat.DD_MM_YYYY))
                            d = LWCalendarUtil.getInstance().dateToString((Date) EntityReflector.getInstance().runGetterMethod(o, ecm.getName()), LWCalendarUtil.DD_MM_YYYY);
                        else if(ecm.getDateFormat().equals(EnumDateFormat.DD_M_YYYY))
                            d = LWCalendarUtil.getInstance().dateToString((Date) EntityReflector.getInstance().runGetterMethod(o, ecm.getName()), LWCalendarUtil.DD_M_YYYY);
                        else if(ecm.getDateFormat().equals(EnumDateFormat.DD_M_YYYY))
                            d = LWCalendarUtil.getInstance().dateToString((Date) EntityReflector.getInstance().runGetterMethod(o, ecm.getName()), LWCalendarUtil.YYYY_MM_DD);

                        rSet[i][j++] = d;
                    }
                    else
                        rSet[i][j++] = EntityReflector.getInstance().runGetterMethod(o, ecm.getName());
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
            i++;
        }

         //Chargement des données dans la table
        //S'il n'y a aucun résultat : prévention contre les erreurs
        if (rSet.length == 0) {
            rSet = new Object[1][colCount];
        }

        LWTableModel model = new LWTableModel(rSet, headers);


        table.setModel(model);
        table.setRowSelectionAllowed(true);
        table.setRowSelectionInterval(0, 0);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setPreferredSize(table.getMaximumSize());

        return rows;
    }
}
