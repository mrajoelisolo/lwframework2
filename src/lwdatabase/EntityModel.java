package lwdatabase;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mitanjo
 */
public class EntityModel {
    private Class entityClass;
    private List<EColumnModel> columns = new ArrayList();

    public EntityModel(Class entityClass) {
        this.entityClass = entityClass;
    }

    public void addColumnString(EColumnModel c) {
        c.setType(EColumnModel.STRING_TYPE);
        columns.add(c);
    }

    public void addColumnDate(EColumnModel c) {
        c.setType(EColumnModel.DATE_TYPE);
        columns.add(c);
    }

    public void addColumnInteger(EColumnModel c) {
        c.setType(EColumnModel.INTEGER_TYPE);
        columns.add(c);
    }

    public void addColumnFloat(EColumnModel c) {
        c.setType(EColumnModel.FLOAT_TYPE);
        columns.add(c);
    }

    public void addColumnImage(EColumnModel c) {
        c.setType(EColumnModel.IMAGE_TYPE);
        columns.add(c);
    }

    public List<EColumnModel> getColumnsModel() {
        return columns;
    }

    public Class getEntityClass() {
        return entityClass;
    }
}
