package lwdatabase;

import java.util.List;
import javax.swing.JTable;

/**
 *
 * @author Mitanjo
 */
public class EntityUtilities {
    private static EntityUtilities instance = new EntityUtilities();

    private EntityUtilities() {}

    public static EntityUtilities getInstance() {
        return instance;
    }

    public IEntity getSelectedElement(JTable table, List<IEntity> data) {
        int i = table.getSelectedRow();
        if(i == -1 || data.size() == 0) return null;

        return data.get(i);
    }
}
