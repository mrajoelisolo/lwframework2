package lwdatabase;

import java.lang.reflect.Method;

/**
 *
 * @author Mitanjo
 */
public class EntityReflector {

    private static EntityReflector instance = new EntityReflector();

    private EntityReflector() {
    }

    public static EntityReflector getInstance() {
        return instance;
    }

    public Object runGetterMethod(Object obj, String methodName) throws Exception {
        Object[] args = null;

        for(Method m : obj.getClass().getMethods()) {
            String getterName = m.getName().substring(3);
            if(getterName.toLowerCase().equals(methodName.toLowerCase()) && m.getName().startsWith("get"))
                return m.invoke(obj, args);
        }

        return null;
    }
}
