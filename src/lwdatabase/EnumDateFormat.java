package lwdatabase;

/**
 *
 * @author Mitanjo
 */
public enum EnumDateFormat {
    DD_MM_YYYY, DD_M_YYYY, YYYY_MM_DD, N_A
}
