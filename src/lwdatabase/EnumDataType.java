package lwdatabase;

/**
 *
 * @author Mitanjo
 */
enum EDataType {
    STRING, INTEGER, FLOAT, DATE
}
