package lwcanvas.shapes;

import java.awt.Color;
import java.awt.Stroke;
import java.awt.BasicStroke;

/**
 * Classe contenant les propriétés de bases d'une shape (Epaisseur des traits et couleur des traits)
 * @author Mitanjo
 * @version 1.1
 */
public abstract class LWAbstractShape {
    protected int penWidth = 1;
    protected Color penColor = Color.GREEN;
    protected Stroke stroke = new BasicStroke(getPenWidth());
    protected float alphaValue = 1f;
    protected boolean visible = true;

    public int getPenWidth() {
        return penWidth;
    }

    public void setPenWidth(int penWidth) {
    	if(this.penWidth != penWidth) {
        	stroke = null; stroke = new BasicStroke(penWidth);
        	this.penWidth = penWidth;
    	}
    }

    public Color getPenColor() {
        return penColor;
    }

    public void setPenColor(Color penColor) {
        this.penColor = penColor;
    }

    public float getAlphaValue() {
        return alphaValue;
    }

    public void setAlphaValue(float alphaValue) {
        this.alphaValue = alphaValue;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
