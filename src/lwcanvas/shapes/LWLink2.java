package lwcanvas.shapes;

import lwcanvas.util.LWGraphicsHelper;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import lwcanvas.components.LWButton2;
import lwcanvas.util.LWAllocator;

/**
 *
 * @author Mitanjo
 * @version 1.0
 * @since 2010-03-09
 */
public class LWLink2 extends LWAbstractShape implements IShape {
    private Point p1 = new Point();
    private Point p2 = new Point();
    private LWCircle c1 = new LWCircle(0, 0, 3);
    private LWCircle c2 = new LWCircle(0, 0, 3);
    private LWPolyline path;
    private boolean reverted = false;
    private LWButton2 btn = null;

    private LWLink2() {
        path = new LWPolyline(LWAllocator.getInstance().allocateArrayPoint(4));
        c1.setFilled(false);
        c2.setFilled(false);
    }

    public LWLink2(LWButton2 btn, int x, int y) {
        this();

        this.btn = btn;
        p2.setLocation(x, y);
    }

    public LWLink2(int x1, int y1, int x2, int y2) {
        this();

        p1.setLocation(x1, y1);
        p2.setLocation(x2, y2);
    }

    private void refresh() {
        if(btn != null)
            p1.setLocation(btn.getLocation().x + btn.getBounds().width, btn.getLocation().y + btn.getBounds().height/2);

        path.setNodeAt(0, p1.x, p1.y);

        if(!reverted) {
            path.setNodeAt(1, p1.x + (p2.x - p1.x)/2, p1.y);
            path.setNodeAt(2, p2.x - (p2.x - p1.x)/2, p2.y);
        }else {
            path.setNodeAt(1, p1.x, p1.y + (p2.y - p1.y)/2);
            path.setNodeAt(2, p2.x, p2.y - (p2.y - p1.y)/2);
        }
        
        path.setNodeAt(3, p2.x, p2.y);

        c1.setLocation(p1.x, p1.y);
        c2.setLocation(p2.x, p2.y);
    }

    @Override
    public void draw(Graphics g) {
        if(!visible || !btn.isFocused()) return;

        refresh();

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        path.setAlphaValue(alphaValue);
        c1.setAlphaValue(alphaValue);
        c2.setAlphaValue(alphaValue);

        c1.setPenColor(penColor);
        c2.setPenColor(penColor);
        path.setPenColor(penColor);

        path.draw(g2);
        c1.draw(g2);
        c2.draw(g2);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    public void setAnchor1(int x, int y) {
        p1.setLocation(x, y);
        refresh();
    }

    public void setAnchor2(int x, int y) {
        p2.setLocation(x, y);
        refresh();
    }

    @Override
    public boolean contains(Point p) {
        return false;
    }

    public boolean isReverted() {
        return reverted;
    }

    public void setReverted(boolean reverted) {
        this.reverted = reverted;
    }
}