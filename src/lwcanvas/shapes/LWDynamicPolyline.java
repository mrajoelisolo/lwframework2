package lwcanvas.shapes;

import lwcanvas.util.LWGraphicsHelper;
import java.awt.Point;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import lwcanvas.util.LWGeom;
import lwcanvas.util.LWImgUtil;

/**
 * Classe permettant de dessiner un polyligne a coordonnées relatives sur un canevas
 * La différence par rapprt a la forme LWPolyline est que l'on peut changer la position
 * du polyligne sans avoir a modifier les coordonnées de chaque noeud
 * Note : Utiliser le polyligne standard (LWPolyline) autant que possible, cette forme est plus lourde a gérer
 * @author Mitanjo
 */
public class LWDynamicPolyline extends LWPolyline {
    private int[] xImg;
    private int[] yImg;
    private Point origin = new Point();
    private boolean isDirty = true;

    public LWDynamicPolyline(List<Point> pts) {
        super(pts);

        toArrayPts(pts);
        xImg = new int[nCount];
        yImg = new int[nCount];
    }

    public LWDynamicPolyline(Point[] pts) {
        super(pts);

        toArrayPts(pts);
        xImg = new int[nCount];
        yImg = new int[nCount];
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        transform();

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);

        g2.setStroke(stroke);
        g2.setColor(penColor);

        g2.drawPolyline(xImg, yImg, nCount);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        return LWGeom.getInstance().contains(xImg, yImg, nCount, p);
    }

    private void transform() {
        if (isDirty) {
            for (int i = 0; i < nCount; i++) {
                xImg[i] = xTab[i] + origin.x;
                yImg[i] = yTab[i] + origin.y;
            }
            isDirty = false;
        }
    }

    public void setLocation(int x, int y) {
        if (origin.x != x && origin.y != y) {
            isDirty = true;
            origin.setLocation(x, y);
        }
    }
}
