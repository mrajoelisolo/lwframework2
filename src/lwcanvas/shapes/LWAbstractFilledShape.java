package lwcanvas.shapes;

import java.awt.Color;
import lwcanvas.util.LWColorProvider;

/**
 * Classe contenant les attributs d'une forme pleine, pouvant être herité par une
 * forme fille (Ex : Rectangle pleine, cercle pleine)
 * @author Mitanjo
 */
public abstract class LWAbstractFilledShape extends LWAbstractShape {
    protected  boolean filled = true;
    protected  Color brushColor = LWColorProvider.getInstance().getColor(0, 128, 0);

    public Color getBrushColor() {
            return brushColor;
    }

    public void setBrushColor(Color brushColor) {
            this.brushColor = brushColor;
    }

    public boolean isFilled() {
            return filled;
    }

    public void setFilled(boolean filled) {
            this.filled = filled;
    }
}
