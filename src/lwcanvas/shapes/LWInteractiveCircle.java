package lwcanvas.shapes;

import lwcanvas.util.LWGraphicsHelper;
import lwcanvas.util.LWGeom;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import lwcanvas.util.LWImgUtil;

/**
 * Classe permettant de dessiner un cercle interactive sur le canevas
 * @author Mitanjo
 * @version 1.2
 */
public class LWInteractiveCircle extends LWAbstractInteractiveShape implements IShape {
    private Rectangle bounds = new Rectangle();

    public LWInteractiveCircle(int x, int y, int radius) {
        super();
        bounds.setBounds(x - radius, y - radius, 2 * radius, 2 * radius);
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);

        if(isFocused()) {
            if(isPressed())
                g2.setColor(getPressedColor());
            else
                g2.setColor(getFocusColor());

            setPenWidth(2);
        }else {
            g2.setColor(getLostFocusColor());
            setPenWidth(1);
        }

        if(filled)
            g2.fillOval(bounds.x, bounds.y, bounds.width, bounds.height);

        g2.setStroke(stroke);
        g2.setColor(penColor);
        g2.drawOval(bounds.x, bounds.y, bounds.width, bounds.height);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        if(!visible) return false;

        if(LWGeom.getInstance().getDistance(p.x, p.y, bounds.x + getRadius(), bounds.y + getRadius()) <= getRadius())
            return true;

       return false;
    }

    public int getRadius() {
        return bounds.width / 2;
    }

    public void setLocation(int x, int y) {
        bounds.setLocation(x - getRadius(), y - getRadius());
    }

    Point pBuf = new Point();
    public Point getLocation() {
        pBuf.setLocation(bounds.x + getRadius(), bounds.y + getRadius());

        return pBuf;
    }
}
