package lwcanvas.shapes;

import lwcanvas.util.LWGraphicsHelper;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import lwcanvas.util.LWImgUtil;

/**
 * Classe permettant de dessiner un rectangle sur le canvas
 * @author Mitanjo
 */
public class LWRectangle extends LWAbstractFilledShape implements IShape {
    protected Rectangle bounds = new Rectangle();

    public LWRectangle() {}

    public LWRectangle(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);

        if(filled) {
            g2.setColor(brushColor);
            g2.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }

        g2.setStroke(stroke);
        g2.setColor(penColor);

        g2.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        if(!visible) return false;

        return bounds.contains(p);
    }

    private Point pBuf = new Point();
    public Point getLocation() {
        pBuf.setLocation(bounds.x, bounds.y);

        return pBuf;
    }

    public void setLocation(int x, int y) {
        bounds.setLocation(x, y);
    }

    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.x = x;
        bounds.y = y;
        bounds.width = wdt;
        bounds.height = hgt;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void setSize(int wdt, int hgt) {
        bounds.setSize(wdt, hgt);
    }
}
