package lwcanvas.shapes;

import java.awt.Color;
import lwcanvas.LWCanvasMouseListener;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import lwcanvas.util.LWColorProvider;

/**
 * Classe contenant les attributs qui permettent a une classe d'être interactive (Ex : LWButton)
 * @author Mitanjo
 */
public abstract class LWAbstractInteractiveShape extends LWAbstractFilledShape {

    protected LWCanvasMouseListener mouseListener;
    private Color focusColor = LWColorProvider.getInstance().getColor(0, 64, 0);
    private Color lostFocusColor = LWColorProvider.getInstance().getColor(0, 16, 0);
    private Color pressedColor =  LWColorProvider.getInstance().getColor(0, 128, 0);
    protected boolean focused = false;
    protected boolean pressed = false;
    protected boolean enabled = true;

    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();

    public LWAbstractInteractiveShape() {
    }

    public void setMouseListener(LWCanvasMouseListener mouseListener) {
        this.mouseListener = mouseListener;
    }

    public abstract boolean contains(Point p);

    public boolean isFocused() {
    	if(!enabled) return false;

        refreshFocusState();
    	
    	if(!isPressed()) {
    		if(mouseListener != null) {
    			IShape sh = mouseListener.getSelection();
    			
    			if(sh != null)
    				if(sh.equals(this)) {
    					launchThread();	
		    			mouseListener.clearSelection();
    				}
    		}
    	}
    	
        return focused;
    }
    
    public boolean isPressed() {
    	return pressed;
    }
    
    private void refreshFocusState() {
    	if (mouseListener != null) {
            Point pt = mouseListener.getCoords();

            if (contains(pt)) {
                focused = true;
                
                if(mouseListener.isLeftClicked())
                	pressed = true;
                else
                	pressed = false;
            } else
                focused = false;
        }
    }
    
    private void launchThread() {
    	Runnable pr = new Runnable() {
            @Override
    		public void run() {
    			actionPerformed();
    		}
    	};
    	
    	Thread th = new Thread(pr);
    	th.start();
    	    	
    	th = null;
    }
    
    public void actionPerformed() {
        for(ActionListener actionListener : actionListeners)
            actionListener.actionPerformed(null);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Color getFocusColor() {
        return focusColor;
    }

    public void setFocusColor(Color focusColor) {
        this.focusColor = focusColor;
    }

    public Color getLostFocusColor() {
        return lostFocusColor;
    }

    public void setLostFocusColor(Color lostFocusColor) {
        this.lostFocusColor = lostFocusColor;
    }

    public Color getPressedColor() {
        return pressedColor;
    }

    public void setPressedColor(Color pressedColor) {
        this.pressedColor = pressedColor;
    }

    public void addActionListener(ActionListener actionListener) {
        this.actionListeners.add(actionListener);
    }
}
