package lwcanvas.shapes;

import java.awt.Graphics;
import java.awt.Point;

/**
 * Interface permettant a une classe d'être dessinée dans un canevas LWCanvas
 * @author Mitanjo
 * @version 1.1
 */
public interface IShape {
    public void draw(Graphics g);
    public boolean contains(Point p);
}
