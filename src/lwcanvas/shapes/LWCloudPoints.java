package lwcanvas.shapes;

import lwcanvas.util.LWGraphicsHelper;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 * @since 26/08/2010
 */
public class LWCloudPoints extends LWAbstractShape implements IShape{
    private boolean bContains = false;
    private List<Point> pts = new ArrayList();
    private int pdSize = 5;
    private Point origin = new Point();

    public LWCloudPoints() {}

    public LWCloudPoints(List<Point> pts) {
        for(Point p : pts)
            this.pts.add(new Point(p));
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        g2.setStroke(stroke);
        g2.setColor(penColor);
        LWImgUtil.getInstance().setComposite(g2, alphaValue);

        for(Point p : pts)
            g2.drawOval(origin.x + p.x - pdSize/2, origin.y + p.y - pdSize/2, pdSize, pdSize);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        return bContains;
    }

    public int getPdSize() {
        return pdSize;
    }

    public void setPdSize(int pdSize) {
        this.pdSize = pdSize;
    }

    public void addNode(int x, int y) {
        this.pts.add(new Point(x, y));
    }

    public void clearNodes() {
        this.pts.clear();
    }

    public List<Point> getNodes() {
        return pts;
    }

    public void setOrigin(int x, int y) {
        origin.setLocation(x, y);
    }

    public Point getOrigin() {
        return origin;
    }
}
