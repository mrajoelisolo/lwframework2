package lwcanvas.shapes.charts;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.List;
import lwcanvas.util.LWGraphicsHelper;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWDynamicPolygon;
import lwcanvas.util.LWAllocator;
import lwcanvas.util.LWGeom;

/**
 *
 * @author Mitanjo
 */
public class LWRadarChart implements IShape {
    private List<LWDataChart> datas;
    private LWDynamicPolygon polyFrame;
    private LWDynamicPolygon polyData;
    private LWDynamicPolygon poly1;
    private LWDynamicPolygon poly2;
    private LWDynamicPolygon poly3;
    private LWDynamicPolygon poly4;
    private LWDynamicPolygon polyKernel;
    private Point center = new Point();
    private Point pBuf = new Point();
    private double scale = 10;
    private int nCount = 0;
    private int maxValue = 0;
    private boolean dirty = true;
    private double theta0 = -Math.PI/2;

    public LWRadarChart(List<LWDataChart> datas) {
        this.datas = datas;

        refreshData();
    }

    private void refreshData() {
        nCount = 0;
        double theta = theta0;

        if(dirty) {
            nCount = datas.size();

            for(int i = 0; i < nCount; i++)
                if(datas.get(i).getValue() > maxValue)
                    maxValue = (int) datas.get(i).getValue();

            polyFrame = null; polyData = null;
            polyFrame = new LWDynamicPolygon(LWAllocator.getInstance().allocateArrayPoint(nCount));
            polyFrame.setFilled(false);
            
            polyData = new LWDynamicPolygon(LWAllocator.getInstance().allocateArrayPoint(nCount));
            polyData.setPenWidth(2);
            polyData.setAlphaValue(0.7f);

            poly1 = new LWDynamicPolygon(LWAllocator.getInstance().allocateArrayPoint(nCount));
            poly1.setFilled(false);
            
            poly2 = new LWDynamicPolygon(LWAllocator.getInstance().allocateArrayPoint(nCount));
            poly2.setFilled(false);

            poly3 = new LWDynamicPolygon(LWAllocator.getInstance().allocateArrayPoint(nCount));
            poly3.setFilled(false);

            poly4 = new LWDynamicPolygon(LWAllocator.getInstance().allocateArrayPoint(nCount));
            poly4.setFilled(false);

            polyKernel = new LWDynamicPolygon(LWAllocator.getInstance().allocateArrayPoint(nCount));
            polyKernel.setFilled(false);

            dirty = false;
        }

        if(polyData == null || polyFrame == null) return;

        for (int i = 0; i < nCount; i++) {
            pBuf.setLocation(center);
            LWGeom.getInstance().translate(pBuf, (int)(datas.get(i).getValue()*scale), 0, theta);
            polyData.setNodeAt(i, pBuf.x, pBuf.y);

            pBuf.setLocation(center);
            LWGeom.getInstance().translate(pBuf, (int)(maxValue*scale), 0, theta);
            polyFrame.setNodeAt(i, pBuf.x, pBuf.y);

            pBuf.setLocation(center);
            LWGeom.getInstance().translate(pBuf, (int)(maxValue*scale/5)*4, 0, theta);
            poly1.setNodeAt(i, pBuf.x, pBuf.y);

            pBuf.setLocation(center);
            LWGeom.getInstance().translate(pBuf, (int)(maxValue*scale/5)*3, 0, theta);
            poly2.setNodeAt(i, pBuf.x, pBuf.y);

            pBuf.setLocation(center);
            LWGeom.getInstance().translate(pBuf, (int)(maxValue*scale/5)*2, 0, theta);
            poly3.setNodeAt(i, pBuf.x, pBuf.y);

            pBuf.setLocation(center);
            LWGeom.getInstance().translate(pBuf, (int)(maxValue*scale/5), 0, theta);
            poly4.setNodeAt(i, pBuf.x, pBuf.y);

            pBuf.setLocation(center);
            LWGeom.getInstance().translate(pBuf, (int)(maxValue*scale/10), 0, theta);
            polyKernel.setNodeAt(i, pBuf.x, pBuf.y);

            theta += 2*Math.PI / nCount;
        }
    }

    @Override
    public void draw(Graphics g) {
        refreshData();

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        polyFrame.draw(g2);
        drawRays(g2);
        poly1.draw(g2);
        poly2.draw(g2);
        poly3.draw(g2);
        poly4.draw(g2);
        polyKernel.draw(g2);
        polyData.draw(g2);

        LWGraphicsHelper.getInstance().saveGraphics(g2);
    }

    private void drawRays(Graphics2D g2) {
        g2.setColor(polyFrame.getPenColor());

        double ang = theta0;
        for(int i = 0; i < nCount; i++) {
            pBuf.setLocation(center);
            LWGeom.getInstance().translate(pBuf, (int)(maxValue*scale), 0, ang);

            g2.drawLine(center.x, center.y, pBuf.x, pBuf.y);

            ang += 2*Math.PI / nCount;
        }
    }

    @Override
    public boolean contains(Point p) {
        return false;
    }

    public void addData(LWDataChart data) {
        datas.add(data);
        dirty = true;
    }

    public void removeData(LWDataChart data) {
        datas.remove(data);
        dirty = true;
    }

    public void clearData() {
        datas.clear();
        dirty = true;
    }

    public int getXLocation() {
        return center.x;
    }

    public int getYLocation() {
        return center.y;
    }

    public void setLocation(int x, int y) {
        dirty = true;
        this.center.setLocation(x, y);
    }

    public int getRadius() {
        return (int)(maxValue * scale);
    }
}
