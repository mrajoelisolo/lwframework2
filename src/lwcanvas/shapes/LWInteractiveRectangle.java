package lwcanvas.shapes;

import lwcanvas.util.LWGraphicsHelper;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import lwcanvas.util.LWImgUtil;

/**
 * Classe premettant de dessiner un rectangle interactif sur le canevas
 * @author Mitanjo
 * @version 1.2
 */
public class LWInteractiveRectangle extends LWAbstractInteractiveShape implements IShape {
    protected Rectangle bounds = new Rectangle();
   
    public LWInteractiveRectangle() {
        super();
    }

    public LWInteractiveRectangle(int x, int y, int wdt, int hgt) {
        super();

        bounds.setBounds(x, y, wdt, hgt);
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        Graphics2D g2 = (Graphics2D) g;
        
        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);

        if(isFocused()) {
            if(isPressed())
                g2.setColor(getPressedColor());
            else
                g2.setColor(getFocusColor());

            setPenWidth(2);
        }else {
            g2.setColor(getLostFocusColor());
            setPenWidth(1);
        }

        if(filled)
            g2.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);

        g2.setStroke(stroke);
        g2.setColor(penColor);
        g2.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        if(!visible) return false;

        return bounds.contains(p);
    }

    public void setLocation(int x, int y) {
        bounds.setLocation(x, y);
    }

    Point pBuf = new Point();
    public Point getLocation() {
        pBuf.setLocation(bounds.x, bounds.y);
        return pBuf;
    }
}
