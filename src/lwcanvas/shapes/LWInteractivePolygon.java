package lwcanvas.shapes;

import lwcanvas.util.LWGraphicsHelper;
import lwcanvas.util.LWGeom;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.List;
import lwcanvas.util.LWImgUtil;

/**
 * Classe permettant de dessiner une polygone interactive sur le canevas
 * @author Mitanjo
 * @version 1.2
 */
public class LWInteractivePolygon extends LWAbstractInteractiveShape implements IShape{
    private int[] xTab;
    private int[] yTab;
    private int nCount;

    public LWInteractivePolygon(List<Point> pts) {
        super();

        toArrayPts(pts);
    }

    public LWInteractivePolygon(Point[] pts) {
        super();

        toArrayPts(pts);
    }

    private void toArrayPts(List<Point> pts) {
        nCount = pts.size();
        xTab = new int[nCount];
        yTab = new int[nCount];

        for(int i = 0; i < nCount; i++) {
            xTab[i] = pts.get(i).x;
            yTab[i] = pts.get(i).y;
        }
    }

    private void toArrayPts(Point[] pts) {
        nCount = pts.length;
        xTab = new int[nCount];
        yTab = new int[nCount];

        for(int i = 0; i < nCount; i++) {
            xTab[i] = pts[i].x;
            yTab[i] = pts[i].y;
        }
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        Graphics2D g2 = (Graphics2D) g;
        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);

        if(isFocused()) {
            if(isPressed())
                g2.setColor(getPressedColor());
            else
                g2.setColor(getFocusColor());

            setPenWidth(2);
        }else {
            g2.setColor(getLostFocusColor());
            setPenWidth(1);
        }

        if(filled)
            g2.fillPolygon(xTab, yTab, nCount);

        g2.setStroke(stroke);
        g2.setColor(penColor);
        g2.drawPolygon(xTab, yTab, nCount);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        if(!visible) return false;

        return LWGeom.getInstance().contains(xTab, yTab, nCount, p);
    }

    public void setNodeAt(int i, int x, int y) {
        if(i < 0 || i >= nCount) return;
        xTab[i] = x;
        yTab[i] = y;
    }
}
