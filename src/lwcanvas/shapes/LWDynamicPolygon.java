package lwcanvas.shapes;

import lwcanvas.util.LWGraphicsHelper;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.List;
import lwcanvas.util.LWGeom;
import lwcanvas.util.LWImgUtil;

/**
 * Classe permettant de dessiner un polygone a coordonnées relatives sur un canevas
 * La différence par rapprt a la forme LWPolygon est que l'on peut changer la position
 * du polygone sans avoir a modifier les coordonnées de chaque noeud
 * Note : Utiliser le polygone standard (LWPolygon) autant que possible, cette forme est plus lourde a gérer
 * @author Mitanjo
 * @version 1.2
 */
public class LWDynamicPolygon extends LWPolygon {
    private int[] xImg;
    private int[] yImg;
    private Point origin = new Point();
    private boolean dirty = true;

    public LWDynamicPolygon(List<Point> pts) {
        super(pts);

        toArrayPts(pts);
        xImg = new int[nCount];
        yImg = new int[nCount];
    }

    public LWDynamicPolygon(Point[] pts) {
        super(pts);

        toArrayPts(pts);
        xImg = new int[nCount];
        yImg = new int[nCount];
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        transform();

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);

        if (filled) {
            g2.setColor(brushColor);
            g2.fillPolygon(xImg, yImg, nCount);
        }

        g2.setStroke(stroke);
        g2.setColor(penColor);

        g2.drawPolygon(xImg, yImg, nCount);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        return LWGeom.getInstance().contains(xImg, yImg, nCount, p);
    }

    private void transform() {
        if (dirty) {
            for (int i = 0; i < nCount; i++) {
                xImg[i] = xTab[i] + origin.x;
                yImg[i] = yTab[i] + origin.y;
            }
            dirty = false;
        }
    }

    public void setLocation(int x, int y) {
        if (origin.x != x || origin.y != y) {
            dirty = true;
            origin.setLocation(x, y);
        }
    }

    public Point getLocation() {
        return origin;
    }

    @Override
    public int getNodeCount() {
        return nCount;
    }
}
