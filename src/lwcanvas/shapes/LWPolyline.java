package lwcanvas.shapes;

import lwcanvas.util.LWGraphicsHelper;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.List;
import lwcanvas.util.LWGeom;
import lwcanvas.util.LWImgUtil;

/**
 * Classe permettant de déssiner une polyligne sur le canevas
 * @author Mitanjo
 */
class LWPolyline extends LWAbstractShape implements IShape {
    protected int[] xTab;
    protected int[] yTab;
    protected int nCount;

    public LWPolyline(List<Point> pts) {
        super();

        toArrayPts(pts);
    }

    public LWPolyline(Point[] pts) {
        super();

        toArrayPts(pts);
    }

    protected void toArrayPts(Point[] pts) {
        nCount = pts.length;
        xTab = new int[nCount];
        yTab = new int[nCount];

        for(int i = 0; i < nCount; i++) {
            xTab[i] = pts[i].x;
            yTab[i] = pts[i].y;
        }
    }

    protected void toArrayPts(List<Point> pts) {
        nCount = pts.size();
        xTab = new int[nCount];
        yTab = new int[nCount];

        for(int i = 0; i < nCount; i++) {
            xTab[i] = pts.get(i).x;
            yTab[i] = pts.get(i).y;
        }
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);

        g2.setStroke(stroke);
        g2.setColor(penColor);

        g2.drawPolyline(xTab, yTab, nCount);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        if(!visible) return false;

        return LWGeom.getInstance().contains(xTab, yTab, nCount, p);
    }

     public void setNodeAt(int i, int x, int y) {
        if(i < 0 || i >= nCount) return;
        xTab[i] = x;
        yTab[i] = y;
    }

    public int getNodeCount() {
        return nCount;
    }
}
