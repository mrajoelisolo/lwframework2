package lwcanvas.shapes;

import lwcanvas.util.LWGraphicsHelper;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import lwcanvas.util.LWImgUtil;

/**
 * Classe permettant de dessiner un cerlce sur le canevas
 * @author Mitanjo
 */
public class LWCircle extends LWAbstractFilledShape implements IShape {
    private Rectangle bounds = new Rectangle();
    
    public LWCircle(int x, int y, int radius) {
        super();
        bounds.setBounds(x - radius, y - radius, 2 * radius, 2 * radius);
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);

		if(filled) {
			g2.setColor(brushColor);
			g2.fillOval(bounds.x, bounds.y, bounds.width, bounds.height);
		}

		g2.setStroke(stroke);
        g2.setColor(penColor);

        g2.drawOval(bounds.x, bounds.y, bounds.width, bounds.height);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }

    public boolean contains(Point p) {
        if(!visible) return false;

        return bounds.contains(p);
    }
    
    private Point pBuf = new Point();
    public Point getLocation() {
        pBuf.setLocation(bounds.x + getRadius(), bounds.y + getRadius());
        return pBuf;
    }

    public void setLocation(int x, int y) {
    	bounds.x = x - bounds.width / 2;
    	bounds.y = y - bounds.height / 2;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public int getRadius() {
        return bounds.width/2;
    }

    public void setRadius(int r) {
        bounds.setSize(r*2, r*2);
    }
}
