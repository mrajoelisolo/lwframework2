package lwcanvas.shapes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import lwcanvas.util.LWGraphicsHelper;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 * @since 15/10/2011
 */
public class LWLine extends LWAbstractShape implements IShape {
    private Point fromPoint = new Point();
    private Point toPoint = new Point();

    public LWLine() {}

    public LWLine(int x1, int x2, int y1, int y2) {
        setLocation(x1, y1, x2, y2);
    }

    public void draw(Graphics g) {
        if(!visible) return;

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        g2.setColor(penColor);
        g2.setStroke(stroke);
        LWImgUtil.getInstance().setComposite(g2, alphaValue);
        
        g2.drawLine(toPoint.x, toPoint.y, fromPoint.x, fromPoint.y);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    public boolean contains(Point p) {
        return false;
    }

    public void setLocation(int x1, int y1, int x2, int y2) {
        toPoint.setLocation(x1, y1);
        fromPoint.setLocation(x2, y2);
    }

    public Point getToPoint() {
        return toPoint;
    }

    public void setToPoint(int x, int y) {
        toPoint.setLocation(x, y);
    }

    public Point getFromPoint() {
        return fromPoint;
    }

    public void setFromPoint(int x, int y) {
        fromPoint.setLocation(x, y);
    }
}
