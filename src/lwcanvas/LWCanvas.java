package lwcanvas;

import lwcanvas.shapes.*;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Iterator;
import java.awt.Point;
import java.awt.RenderingHints;

/**
 *
 * @author Mitanjo
 * @version 1.1
 * Classe canevas permettant de dessiner des formes (ou shape, tout ce qui est implémenté par IShape)
 */
public class LWCanvas extends JPanel {
    private LWCanvasMouseListener mouseListener;
    
    protected int renderQuality = 0;
    public static int RENDER_LOW_QUALITY = 0;
    public static int RENDER_HIGH_QUALITY = 1;
    
    protected int antialiasingMode = 0;
    public static int ANTIALIASING_OFF = 2;
    public static int ANTIALIASING_ON = 3;

    protected List<IShape> shapes = new ArrayList();

    public LWCanvas() {
        super();

        mouseListener = new LWCanvasMouseListener(this);
        setBackground(Color.BLACK);
    }

    @Override
    /**
     * Déssine tous les composants dans le conteneur ainsi que les shapes qui sont contenus
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        paintBackground(g);

	Graphics2D g2 = (Graphics2D) g;
        
        //Définir la qualité du rendu
        if(renderQuality == RENDER_LOW_QUALITY )
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        else if(renderQuality == RENDER_HIGH_QUALITY)
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
        else
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_DEFAULT);
        
        //Définir le mode antialiasing
        if(antialiasingMode == ANTIALIASING_OFF) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        }
        else if(antialiasingMode == ANTIALIASING_ON) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }
        else {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_DEFAULT);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT);
        }

        for(IShape sh : shapes)
            sh.draw(g2);
    }

    /**
     * Dessine le fond du canvas
     * @param g
     */
    public void paintBackground(Graphics g) {}

    @Override
    public void setBackground(Color bg) {
        Color oldBg = getBackground();
        super.setBackground(bg);
        if ((oldBg != null) ? !oldBg.equals(bg) : ((bg != null) && !bg.equals(oldBg))) {
            // background already bound in AWT1.2
            repaint();
        }
    }

    /**
     * Ajouter un shape implémentant IShape dans le canevas
     * @param sh
     */
    public void addShape(IShape sh) {
        if(shapes.contains(sh)) return;
        
        shapes.add(sh);

        if(sh instanceof LWAbstractInteractiveShape) {
            LWAbstractInteractiveShape sh2 = (LWAbstractInteractiveShape) sh;
            sh2.setMouseListener(mouseListener);
        }
    }

    public void removeShape(IShape sh) {
        if(!shapes.contains(sh)) return;

        shapes.remove(sh);
    }

    /**
     * Effacer tous les shapes du canevas
     */
    public void clearShapes() {
        if(shapes.size() == 0) return;

        shapes.clear();
    }
    
    private List bSel = new ArrayList();
    /**
     * Obtenir la liste des tous les shapes du canevas
     * @param p
     * @return
     */
    public List<IShape> getShapes(Point p) {
    	bSel.clear();
    	
    	for(Iterator iter = shapes.iterator(); iter.hasNext();) {
    		IShape sh = (IShape) iter.next();
    		if(sh.contains(p))
    			bSel.add(sh);
    	}
    	
    	return bSel;
    }

    /**
     * Obtenir la liste des shapes du canevas
     * @return
     */
    public List<IShape> getShapes() {
        return shapes;
    }

    /**
     * Définir la qualité du rendu affiché dans le canevas
     * @param value
     */
    public void setRenderQuality(int value) {
        if(value != RENDER_LOW_QUALITY && value != RENDER_HIGH_QUALITY)
            System.err.println("LWCanvas: Incorrect antialiasing parameter");

        renderQuality = value;
    }

    /**
     * Définit la qualité de l'anti-crénélage affiché dans le canevas
     * @param value
     */
    public void setAntialiasing(int value) {
        if(value != ANTIALIASING_ON && value != ANTIALIASING_OFF)
            System.err.println("LWCanvas: Incorrect antialiasing parameter");

        antialiasingMode  = value;
    }
}
