package lwcanvas.util;

import java.awt.Composite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import javax.sound.sampled.Clip;

/**
 * Classe permettant de restaurer et sauvegarder les paramètres d'une instance Graphics2D
 * @author Mitanjo
 * @version 1.0
 */
public class LWGraphicsHelper {
    private static LWGraphicsHelper instance = new LWGraphicsHelper();
    private LWGraphicsHelper() {}
    public static LWGraphicsHelper getInstance() {
        return instance;
    }

    private Color color;
    private Stroke stroke;
    private Font font;
    private Composite alpha;
    private Shape clip;

    public void saveGraphics(Graphics2D g2) {
        color = g2.getColor();
        stroke = g2.getStroke();
        font = g2.getFont();
        alpha = g2.getComposite();
        clip = g2.getClip();
    }

    public void restoreGraphics(Graphics2D g2) {
        g2.setColor(color);
        g2.setStroke(stroke);
        g2.setFont(font);
        g2.setComposite(alpha);
        g2.setClip(clip);
    }
}
