package lwcanvas.util;

import java.awt.Color;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Classe permettant d'obtenir une instance de couleur java.awt.Color
 * a partir des paramètres rouge (r), vert (g) et bleu(b)
 * Si la couleur a été déja instanciée, il la retourne simplement
 * Sinon il crée la couleur en utilisant new java.awt.Color(r, g, b)
 * Les valeurs de r, g, b sont comprises entre 0 et 255 soit 16 bits
 * @author Mitanjo
 */
public class LWColorProvider {	
    private static LWColorProvider instance = new LWColorProvider();

    private LWColorProvider() {}

    public static LWColorProvider getInstance() {
        return instance;
    }

    private List colors = new ArrayList();

    public Color getColor(int r, int g, int b) {
        for(Iterator iter = colors.iterator(); iter.hasNext();) {
            Color col = (Color) iter.next();

            if(col.getRed() == r && col.getGreen() == g && col.getBlue() == b)
                return col;
        }

        Color cl = new Color(r, g, b);
        colors.add(cl);
        return cl;
    }
}
