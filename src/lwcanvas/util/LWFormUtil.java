package lwcanvas.util;

import java.awt.Dimension;
import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * Classe permettant de centrer une formulaire de type JFrame ou JPanel
 * @author Mitanjo
 */
public class LWFormUtil {
    private static LWFormUtil instance;

    private LWFormUtil() {}
    public static LWFormUtil getInstance() {
        if(instance == null)
            instance = new LWFormUtil();

        return instance;
    }

    public void centerForm(JFrame frame) {
        Dimension d = java.awt.Toolkit.getDefaultToolkit().getScreenSize();

        frame.setLocation((d.width - frame.getBounds().width) / 2, (d.height - frame.getBounds().height) / 2);
    }

    public void centerForm(JDialog frame) {
        Dimension d = java.awt.Toolkit.getDefaultToolkit().getScreenSize();

        frame.setLocation((d.width - frame.getBounds().width) / 2, (d.height - frame.getBounds().height) / 2);
    }
}
