package lwcanvas.util;

import javax.swing.UIManager;

/**
 *
 * @author Mitanjo
 */
public class LWLookNFeelManager {
    private static LWLookNFeelManager instance;
    private LWLookNFeelManager() {}

    public static LWLookNFeelManager getInstance() {
        if(instance == null)
            instance = new LWLookNFeelManager();

        return instance;
    }

    public void makeNimbusLookNFeel() {
        try {
            UIManager.setLookAndFeel(
            UIManager.getCrossPlatformLookAndFeelClassName());
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
