package lwcanvas.util;

import java.util.Random;

/**
 *
 * @author Mitanjo
 */
public class Randomizer {
    public static int PRECISION_VALUE = 3;
    private static Randomizer instance = new Randomizer();
    private Random random = new Random();

    private Randomizer() {}

    public static Randomizer getInstance() {
        return instance;
    }

    public double getRandomValue(double minRange, double maxRange) {
        double res = random.nextDouble();

        res = res - 0.5d;
        res = res*Math.abs(maxRange - minRange);

        return res;
    }
}
