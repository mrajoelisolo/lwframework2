package lwcanvas.util;

import java.awt.Point;
import java.io.*;
import javax.swing.JOptionPane;

/**
 * Classe permettant de convertir un fichier en ensemble de points
 * @author Mitanjo
 */
public class LWPointUtilities {
    //Singleton Pattern
    private static LWPointUtilities instance;
    
    private LWPointUtilities() {}
    
    public static LWPointUtilities getInstance() {
        if(instance == null) 
            instance = new LWPointUtilities();
        return instance;
    }
    
    public int getRowCount(String filePath, String separator) {
        int count = 0;
        
        try{
            BufferedReader file = new BufferedReader(new FileReader(filePath));
            
            while(file.readLine() != null)
                    count++;

        }catch(Exception e) {
            e.printStackTrace();
        }
        
        return count;
    }

    public int getRowCount(File file, String separator) {
        int count = 0;

        try{
            BufferedReader reader = new BufferedReader(new FileReader(file));

            while(reader.readLine() != null)
                    count++;

        }catch(Exception e) {
            e.printStackTrace();
        }

        return count;
    }
    
    public Point[] fileToPoints(String filePath, String separator) {
        Point[] pts = null;
        
        try {
            String line;
            int xPos, yPos;
            pts = new Point[getRowCount(filePath, separator)];
            BufferedReader file = new BufferedReader(new FileReader(filePath));
            
            int k = 0;
            while((line = file.readLine()) != null) {
                String[] splitRes = line.split(separator);
                xPos = (int) Float.parseFloat(splitRes[0]);
                yPos = (int) Float.parseFloat(splitRes[1]);
                pts[k] = new Point(xPos, yPos);
                k++;
            }
            
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        
        return pts;
    }

    public Point[] fileToPoints(File file, String separator) {
        Point[] pts = null;

        try {
            String line;
            int xPos, yPos;
            pts = new Point[getRowCount(file, separator)];
            BufferedReader reader = new BufferedReader(new FileReader(file));

            int k = 0;
            while((line = reader.readLine()) != null) {
                String[] splitRes = line.split(separator);
                xPos = (int) Float.parseFloat(splitRes[0]);
                yPos = (int) Float.parseFloat(splitRes[1]);
                pts[k] = new Point(xPos, yPos);
                k++;
            }

        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return pts;
    }
}
