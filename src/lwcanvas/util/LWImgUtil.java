package lwcanvas.util;

import java.awt.AWTException;
import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Cette classe permet de gérer les fichiers images de type JPG, PNG ou GIF.
 * Elle incorpore des méthodes permettant de charger des images et de les 
 * stocker dans des variables de type java.awt.BufferedImage. Outre elle peut
 * effectuer des modifications tels que la desaturation
 * @author Mitanjo
 */
public class LWImgUtil {

    private static LWImgUtil instance = new LWImgUtil();

    private LWImgUtil() {
    }

    public static LWImgUtil getInstance() {
        return instance;
    }

    /**
     * Permet de désaturer l'image c'est-à-dire que l'image est transformé
     * en niveaux de gris
     * @param img
     * L'image a désaturer
     * @return
     * Retourne une copie de l'image mais desaturée
     */
    public BufferedImage desaturate(BufferedImage img) {
        BufferedImage imgDest = img;

        ColorSpace gris = ColorSpace.getInstance(ColorSpace.CS_GRAY);
        ColorSpace couleur = ColorSpace.getInstance(ColorSpace.CS_sRGB);
        ColorConvertOp ccOp = new ColorConvertOp(couleur, gris, null);
        ccOp.filter(imgDest, imgDest);

        return imgDest;
    }

    /**
     * Permet de charger une image de type JPG, PNG ou GIF
     * @param fileName - le fichier à charger. Si vous ne spécifiez directement
     * le fichier sans préciser la racine ou le dossier où il se trouve, alors
     * l'emplacement par défaut sera le dossier où se trouve votre projet
     * @return
     * Une instance de type java.awt.BufferedImage contenant l'image
     */
    public BufferedImage loadImage(String fileName) {
        BufferedImage img = null;

        try {
            img = ImageIO.read(new File(fileName));

        } catch (IOException e) {
            e.printStackTrace();
        }

        return img;
    }

    public BufferedImage filetoImage(File file) {
        BufferedImage img = null;

        try {
            img = ImageIO.read(file);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return img;
    }

    public void saveImage(BufferedImage img, String path) {
        try {
            ImageIO.write(img, "jpg", new File(path));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] imgToBytes(BufferedImage img) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] res = null;

        try {
            ImageIO.write(img, "jpg", baos);
            res = baos.toByteArray();
            baos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }

    public BufferedImage bytesToImg(byte[] byteArray) {
        BufferedImage res = toBufferedImage(Toolkit.getDefaultToolkit().createImage(byteArray));

        res.getWidth(null);

        return res;
    }

    public BufferedImage capturePanel(JPanel panel) {
        Robot robot;
        BufferedImage img = null;

        try {
            robot = new Robot();
            img = robot.createScreenCapture(panel.getBounds());

        } catch (AWTException ex) {
            Logger.getLogger(LWImgUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return img;
    }

    public void setComposite(Graphics2D g2, float alphaValue) {
        float szAlphaValue = alphaValue;

        if(szAlphaValue < 0f)
            szAlphaValue = 0f;
        else if(szAlphaValue > 1f)
            szAlphaValue = 1f;

        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, szAlphaValue));
    }

    /**
     *
     * @param bounds
     * @since 03/03/2010
     * @return
     */
    public BufferedImage captureImage(JFrame frame, JPanel panel, Rectangle bounds) {
        Robot robot;
        BufferedImage img = null;

        try {            
            Rectangle cb = new Rectangle();
            cb.setBounds(bounds);
            cb.setLocation(frame.getX() + panel.getX() + bounds.x, frame.getY() + 27 + panel.getY() + bounds.y);
            
            robot = new Robot();
            img = robot.createScreenCapture(cb);

        } catch (AWTException ex) {
            Logger.getLogger(LWImgUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return img;
    }

    public BufferedImage captureImage(JFrame frame, Rectangle bounds) {
        Robot robot;
        BufferedImage img = null;
        int sp = 0;

        try {
            if(!frame.isUndecorated()) sp = 28;
            Rectangle cb = new Rectangle();
            cb.setBounds(bounds);
            cb.setLocation(frame.getX() + bounds.x, frame.getY() + sp + bounds.y);

            robot = new Robot();
            img = robot.createScreenCapture(cb);

        } catch (AWTException ex) {
            Logger.getLogger(LWImgUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return img;
    }

    /*  public BufferedImage bytesToImg(byte[] bytes)
    BufferedImage bufferedImage = new BufferedImage (1024, 1024, BufferedImage.TYPE_INT_RGB);
    File destImage = new File("img\\Hinata.jpg");
    ImageIO.setUseCache(false);
    bufferedImage = ImageIO.read (new ByteArrayInputStream(bytes));
    System.out.println("\n Before file write");
    ImageIO.write(bufferedImage, "JPEG", destImage);

    try{

    }*//*  public static BufferedImage bytesToImg(byte[] bytes)
    BufferedImage bufferedImage = new BufferedImage (1024, 1024, BufferedImage.TYPE_INT_RGB);
    File destImage = new File("img\\Hinata.jpg");
    ImageIO.setUseCache(false);
    bufferedImage = ImageIO.read (new ByteArrayInputStream(bytes));
    System.out.println("\n Before file write");
    ImageIO.write(bufferedImage, "JPEG", destImage);

    try{

    }*/
    
    /**
     * Convertir une instance Image en BufferedImage
     * source : http://www.dreamincode.net/code/snippet1076.htm
     * @param image
     * @return
     */
    public BufferedImage toBufferedImage(Image image) {
        if (image instanceof BufferedImage) {
            return (BufferedImage) image;
        }

        // This code ensures that all the pixels in the image are loaded
        image = new ImageIcon(image).getImage();

        // Determine if the image has transparent pixels; for this method's
        // implementation, see Determining If an Image Has Transparent Pixels
        boolean hasAlpha = hasAlpha(image);

        // Create a buffered image with a format that's compatible with the screen
        BufferedImage bimage = null;
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        try {
            // Determine the type of transparency of the new buffered image
            int transparency = Transparency.OPAQUE;
            if (hasAlpha) {
                transparency = Transparency.BITMASK;
            }

            // Create the buffered image
            GraphicsDevice gs = ge.getDefaultScreenDevice();
            GraphicsConfiguration gc = gs.getDefaultConfiguration();
            bimage = gc.createCompatibleImage(image.getWidth(null), image.getHeight(null), transparency);
        } catch (HeadlessException e) {
            // The system does not have a screen
        }

        if (bimage == null) {
            // Create a buffered image using the default color model
            int type = BufferedImage.TYPE_INT_RGB;
            if (hasAlpha) {
                type = BufferedImage.TYPE_INT_ARGB;
            }
            bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
        }

        // Copy image to buffered image
        Graphics g = bimage.createGraphics();

        // Paint the image onto the buffered image
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return bimage;
    }

    //source : http://www.exampledepot.com/egs/java.awt.image/Image2Buf.html
    public boolean hasAlpha(Image image) {
        // If buffered image, the color model is readily available
        if (image instanceof BufferedImage) {
            return ((BufferedImage) image).getColorModel().hasAlpha();
        }

        // Use a pixel grabber to retrieve the image's color model;

        // grabbing a single pixel is usually sufficient

        PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);

        try {
            pg.grabPixels();
        } catch (InterruptedException e) {
        }

        // Get the image's color model

        return pg.getColorModel().hasAlpha();

    }
}  
