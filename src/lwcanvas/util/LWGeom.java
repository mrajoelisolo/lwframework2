package lwcanvas.util;

import java.awt.Point;

/**
 * Classe contenant des fonctions utilisées en géometrie
 * @author FireWolf
 */
public class LWGeom {

    private static LWGeom instance;

    private LWGeom() {}

    public static LWGeom getInstance() {
        if (instance == null) {
            instance = new LWGeom();
        }
        return instance;
    }

    /**
     * Obtenir la distance entre deux points P1 et P2
     * @param x1
     * Abscisse du point P1
     * @param y1
     * Ordonnée du point P1
     * @param x2
     * Abscisse du point P2
     * @param y2
     * Ordonnée du point P2
     * @return
     * Valeur de la distance entre les deux points
     */
    public double getDistance(int x1, int y1, int x2, int y2) {
        double res = 0;

        res = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

        return res;
    }

    /**
     * Vérifie si un point M se trouve a l'intérieur d'une polygone formé par
     * les ensembles de points P(n) = (xTab(n), yTab(n))
     * @param xTab
     * Ensemble des abscisses des points P(n)
     * @param yTab
     * Ensemble des ordonnées des points P(n)
     * @param nCount
     * Nombre de points formant le polygone
     * @param p
     * Instance d'un java.awt.Point contenant les coordonnées de M
     * @return
     */
    public boolean contains(int[] xTab, int[] yTab, int nCount, Point p) {
        double angle = 0;
        double theta1, theta2, dTheta;
        
        for(int i = 0; i < nCount; i++) {
            theta1 = Math.atan2(yTab[i] - p.y, xTab[i] - p.x);
            theta2 = Math.atan2(yTab[(i+1)%nCount] - p.y, xTab[(i+1)%nCount] - p.x);
            dTheta = theta2 - theta1;
            
            while(dTheta > Math.PI) dTheta -= 2*Math.PI;
            while(dTheta < -Math.PI) dTheta += 2*Math.PI;
            angle += dTheta;
        }
        
        return Math.abs(angle) > Math.PI;
    }

    /**
     * Obtenir l'angle formé par 2 formé par les droites des  points P1 et P2 et une droite horizontale
     * @param x1
     * Abscisse du point P1
     * @param y1
     * Ordonnée du point P1
     * @param x2
     * Abscisse du point P2
     * @param y2
     * Ordonnée du point P2
     * @return
     * Valeur de l'angle formé par les deux droites
     */
    public double getAngle(int x1, int y1, int x2, int y2) {
        return Math.atan2(y2 - y1, x2 - x1);
    }

    /**
     * Retourne l'angle formé par les vecteurs (o, p1) et (o, p2)
     * @param o
     * @param p1
     * @param p2
     * @return
     * Angle formé par les trois points en radians
     */
    public double getAngle(Point o, Point p1, Point p2) {
        double res = Math.atan2(p2.y - o.y, p2.x - o.x) - Math.atan2(p1.y - o.y, p1.x - o.x);
        
        while(res > Math.PI) res -= 2*Math.PI;
        while(res < -Math.PI) res += 2*Math.PI;

        return res;
    }

    /**
     * Faire la translation d'un point de vecteurs orthogonaux vx et vy dont l'angle
     * formé par vx et l'horizontale est égale a theta
     * @param p
     * Le point a translater
     * @param r
     * Norme de vx
     * @param d
     * Norme de vy
     * @param theta
     */
    public void translate(Point p, int r, int d, double theta) {
        double tx, ty;

        tx = r * Math.cos(theta) - d * Math.sin(theta);
        ty = r * Math.sin(theta) + d * Math.cos(theta);

        p.setLocation(p.x + tx, p.y + ty);
    }

    public void translate(Point p, double r, double d, double theta) {
        double tx, ty;

        tx = r * Math.cos(theta) - d * Math.sin(theta);
        ty = r * Math.sin(theta) + d * Math.cos(theta);

        p.setLocation(p.x + tx, p.y + ty);
    }

    public double toDeg(double rad) {
        return 180*rad/Math.PI;
    }
}
