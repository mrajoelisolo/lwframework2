package lwcanvas.util;

import java.awt.Font;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class LWFontProvider {
	private static LWFontProvider instance = new LWFontProvider();
	
	private LWFontProvider() {}
	
	public static LWFontProvider getInstance() {
            return instance;
	}
	
	private List fonts = new ArrayList();
	
	public Font getFont(String fontName, int fontType, int fontSize) {
            for(Iterator iter = fonts.iterator(); iter.hasNext();) {
                Font font = (Font) iter.next();

                if(font.getFontName().equals(fontName) && font.getSize() == fontSize && fontTypeEquals(fontType, font)) {
                    return font;
		}
            }
            
            Font font = new Font(fontName, fontType, fontSize);
            fonts.add(font);
            return font;
	}
	
	private boolean fontTypeEquals(int fontType, Font font) {
		if(font.isBold())
			if(fontType == Font.BOLD)
				return true;
			else
				return false;
				
		if(font.isItalic())
			if(fontType == Font.ITALIC)
				return true;
			else
				return false;
				
		if(font.isPlain())
			if(fontType == Font.PLAIN)
				return true;
			else
				return false;
				
		return false;
	}
	
	public static final Font BTN_FONT_NORMAL = new Font("Arial Rounded MT Bold", Font.PLAIN, 15);
        public static final Font BTN_FONT_COURIER_NORMAL_20 = new Font("Courier New", Font.PLAIN, 20);
        public static final Font BTN_FONT_COURIER_BOLD_20 = new Font("Courier New", Font.BOLD, 20);
}
