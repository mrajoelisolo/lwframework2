package lwcanvas.util;

import java.awt.Point;

/**
 *
 * @author Mitanjo
 */
public class LWAllocator {
    private static LWAllocator instance = new LWAllocator();

    private LWAllocator() {}

    public static LWAllocator getInstance() {
        return instance;
    }

    public Point[] allocateArrayPoint(int n) {
        Point[] res = new Point[n];
        
        for(int i = 0; i < n; i++) {
            res[i] = new Point();
        }

        return res;
    }
}
