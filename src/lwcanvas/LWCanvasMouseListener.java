package lwcanvas;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.List;
import lwcanvas.shapes.IShape;

/**
 *
 * @author Mitanjo
 * @version 1.1
 * Classe spécialisé permettant de gérer les écoutes d'évènements de la souris sur un canevas (LWCanvas)
 */
public class LWCanvasMouseListener extends LWCanvasMouseAdapter {
    private List selection;
    private Point coords = new Point();
    private boolean leftClicked = false;
    private boolean rightClicked = false;

    public LWCanvasMouseListener(LWCanvas canvas) {
        super(canvas);
    }

    @Override
    public void leftClickAction(MouseEvent e) {
        leftClicked = true;
    }

    @Override
    public void rightClickAction(MouseEvent e) {
        rightClicked = false;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        coords.setLocation(e.getPoint());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    	/*I've added this method to avoid a particular bug; When you hold the left click 
    	 *on the shape and leaving the shape, this last would'not be focused*/
    	coords.setLocation(e.getPoint());
    	
        selection = this.canvas.getShapes(e.getPoint());
        
        leftClicked = false;
        rightClicked = false;
    }

    public Point getCoords() {
        return coords;
    }

    public boolean isLeftClicked() {
        return leftClicked;
    }

    public boolean isRightClicked() {
        return rightClicked;
    }
    
    public IShape getSelection() {
    	if(selection != null) {
    		if(selection.size() > 0)
    			return (IShape) selection.get(0);
    		else
    			return null;
    	}
    	
    	return null;
    }
    
    public void clearSelection() {
    	if(selection != null)
    		selection.clear();
    }
}
