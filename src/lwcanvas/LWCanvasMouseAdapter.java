package lwcanvas;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingUtilities;

/**
 *
 * @author Mitanjo
 * Classe adaptateur permettant de gérer les écoutes d'évènements de la souris sur un canevas (LWCanvas)
 */
public abstract class LWCanvasMouseAdapter extends MouseAdapter {
    protected LWCanvas canvas;

    public LWCanvasMouseAdapter(LWCanvas canvas) {
        super();
        this.canvas = canvas;
        canvas.addMouseListener(this);
        canvas.addMouseMotionListener(this);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(SwingUtilities.isLeftMouseButton(e))
            leftClickAction(e);
        else if(SwingUtilities.isRightMouseButton(e))
            rightClickAction(e);
    }

    public abstract void leftClickAction(MouseEvent e);

    public abstract void rightClickAction(MouseEvent e);
}
