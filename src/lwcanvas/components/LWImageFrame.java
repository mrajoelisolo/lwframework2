package lwcanvas.components;

import lwcanvas.util.LWGraphicsHelper;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractShape;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import lwcanvas.util.LWImgUtil;

/**
 * Composant dessinant une image encadré
 * @author Mitanjo
 */
public class LWImageFrame extends LWAbstractShape implements IShape {
    protected Rectangle bounds = new Rectangle();
    protected boolean bordered = true;
    protected boolean grid = false;
    protected Color gridColor = penColor;
    protected int horDiv = 8;
    protected int verDiv = 8;
    protected BufferedImage img;

    public LWImageFrame(Image img) {
        this(img, 0, 0, 0, 0);
    }

    public LWImageFrame(Image img, int x, int y, int wdt, int hgt) {
        super();

        bounds.setBounds(x, y, wdt, hgt);
        this.img = LWImgUtil.getInstance().toBufferedImage(img);
    }

    public LWImageFrame(BufferedImage img, int x, int y, int wdt, int hgt) {
        super();

        bounds.setBounds(x, y, wdt, hgt);
        this.img = img;
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        if(img != null) {
            LWImgUtil.getInstance().setComposite(g2, alphaValue);
            g2.drawImage(img, bounds.x, bounds.y, bounds.width, bounds.height, null);
        }

        if(grid) {
            Composite cTmp = g2.getComposite();
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .3f));

            g2.setColor(gridColor);
            int pos = bounds.x + bounds.width / horDiv;

            for(int i = 0; i < horDiv-1; i++) {
                g2.drawLine(pos, bounds.y, pos, bounds.y + bounds.height);
                pos += bounds.width / horDiv;
            }

            pos = bounds.y + bounds.height / verDiv;
            for(int i = 0; i < verDiv-1; i++) {
                g2.drawLine(bounds.x, pos, bounds.x + bounds.width, pos);
                pos += bounds.height / verDiv;
            }

            g2.setComposite(cTmp);
        }

        if(bordered) {
            g2.setStroke(stroke);
            g2.setColor(penColor);
            g2.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        return false;
    }

    public void setGrid(boolean status) {
        grid = status;
    }

    public void setGridColor(Color gridColor) {
        this.gridColor = gridColor;
    }

    public void setBordered(boolean bordered) {
        this.bordered = bordered;
    }

    public void setImage(BufferedImage img) {
        this.img = img;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }

    public void setLocation(int x, int y) {
        bounds.setLocation(x, y);
    }

    private Point pBuf = new Point();
    public Point getLocation() {
        pBuf.setLocation(bounds.x, bounds.y);

        return pBuf;
    }
}