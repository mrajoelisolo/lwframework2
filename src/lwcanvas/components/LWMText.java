package lwcanvas.components;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import lwcanvas.util.LWGraphicsHelper;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractShape;
import lwcanvas.util.LWFontProvider;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 */
public class LWMText extends LWAbstractShape implements IShape {
    private Point pos = new Point();
    private StringBuffer str = new StringBuffer();
    private Font font = LWFontProvider.BTN_FONT_NORMAL;
    private int rowLimit = 12;

    public LWMText() {}

    public LWMText(String text) {
        str.append(text);
    }

    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);
        g2.setColor(penColor);
        g2.setFont(font);
        drawString(g2);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    private void drawString(Graphics2D g2) {
        String[] strs = str.toString().split("\n");
        int yPos = 0;
        for(String s : strs) {
            g2.drawString(s, pos.x, pos.y + yPos);
            yPos += font.getSize() + 5;
        }
    }

    @Override
    public boolean contains(Point p) {
        return false;
    }

    public void setLocation(int x, int y) {
        pos.setLocation(x, y);
    }

    public void append(String str) {
        if(str.length() >= this.str.capacity()
                || this.str.toString().split("\n").length >= rowLimit)
                    clearText();

        this.str.append(str);
    }

    public void clearText() {
        if(str.length() == 0) return;
        str.delete(0, str.length()-1);
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public int getRowLimit() {
        return rowLimit;
    }

    public void setRowLimit(int rowLimit) {
        this.rowLimit = rowLimit;
    }

    public String getText() {
        return str.toString();
    }
}
