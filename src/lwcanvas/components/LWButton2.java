package lwcanvas.components;

import java.awt.Color;
import java.awt.Font;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractInteractiveShape;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import lwcanvas.util.LWGraphicsHelper;
import lwcanvas.util.LWFontProvider;
import java.awt.Image;
import lwcanvas.util.LWImgUtil;

/**
 * Composant bouton graphique pouvant être dessiné dans un LWCanvas
 * @author Mitanjo
 * @version 1.1
 */
public class LWButton2 extends LWAbstractInteractiveShape implements IShape {
    private Rectangle bounds = new Rectangle();
    private LWButtonSkin btnSkin;
    private Image focusedImg;
    private Image noFocusImg;
    private Image pressedImg;
    private Image disabledImg;
    private Image currentImg;
    private String caption;
    private boolean textVisible = true;
    private Color foreColor = Color.BLACK;
    private Font fontFocus = LWFontProvider.BTN_FONT_COURIER_BOLD_20;
    private Font fontLostFocus = LWFontProvider.BTN_FONT_COURIER_BOLD_20;

    public LWButton2() {
        this("");
    }

    public LWButton2(String caption) {
        super();
        this.caption = caption;
    }

    public void setBtnSkin(LWButtonSkin btnSkin) {
        this.btnSkin = btnSkin;
    }

    @Override
    public void draw(Graphics g) {
        if(!visible) return;

        Graphics2D g2 = (Graphics2D) g;
        
        LWGraphicsHelper.getInstance().saveGraphics(g2);
        LWImgUtil.getInstance().setComposite(g2, alphaValue);
        
        if(btnSkin != null)
            if(btnSkin.isOk()) {
                paint1(g2);
                return;
            }

        paint2(g2);
        
        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    private void paint1(Graphics2D g2) {
        this.noFocusImg = btnSkin.getNoFocusImg();
        this.focusedImg = btnSkin.getFocusedImg();
        this.pressedImg = btnSkin.getPressedImg();
        this.disabledImg = btnSkin.getDisabledImg();

        if(isFocused()) {
            if(isPressed())
                currentImg = pressedImg;
            else
                currentImg = focusedImg;

            g2.setFont(fontFocus);
        } else {
            currentImg = noFocusImg;
            g2.setFont(fontLostFocus);
        }

        if(disabledImg != null)
            if(!isEnabled()) {
                currentImg = disabledImg;
            }        

        g2.drawImage(currentImg, bounds.x, bounds.y, bounds.width, bounds.height, null);

        if(textVisible && caption != null) {
            g2.setColor(foreColor);
            //g2.setFont(fontFocus);
            g2.drawString(caption, bounds.x + 10, bounds.y + bounds.height/2 + 5);
        }
    }

    private void paint2(Graphics2D g2) {
        //Added since 14/05/11
        if(!enabled) {
            g2.setColor(Color.GRAY);
            g2.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);

            g2.setColor(Color.DARK_GRAY);
            g2.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);

            if(textVisible) {
                g2.setFont(fontFocus);
                g2.drawString(caption, bounds.x + 10, bounds.y + bounds.height/2 + 5);
            }

            return;
        }

        if(isFocused()) {
            if(isPressed())
                g2.setColor(getPressedColor());
            else
                g2.setColor(getFocusColor());

            g2.setFont(fontFocus);
            setPenWidth(2);
        }else {
            g2.setColor(getLostFocusColor());
            g2.setFont(fontLostFocus);
            setPenWidth(1);
        }

        g2.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);

        g2.setStroke(stroke);
        g2.setColor(penColor);
        g2.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);

        if(textVisible) {
            g2.setColor(penColor);
            g2.drawString(caption, bounds.x + 10, bounds.y + bounds.height/2 + 5);
        }
    }

    @Override
    public boolean contains(Point p) {
        if(!visible) return false;

        return bounds.contains(p);
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }

    public String getText() {
        return caption;
    }

    public void setText(String text) {
        this.caption = text;
    }

    public void setTextVisible(boolean textVisible) {
        this.textVisible = textVisible;
    }

    public boolean isTextVisible() {
        return textVisible;
    }

    public void setLocation(int x, int y) {
        bounds.setLocation(x, y);
    }

    private Point pBuf = new Point();
    public Point getLocation() {
        pBuf.setLocation(bounds.x, bounds.y);

        return pBuf;
    }

    public Color getForeColor() {
        return foreColor;
    }

    public void setForeColor(Color foreColor) {
        this.foreColor = foreColor;
    }

    public Font getFontFocus() {
        return fontFocus;
    }

    public void setFontFocus(Font font) {
        this.fontFocus = font;
    }

    public Font getFontLostFocus() {
        return fontLostFocus;
    }

    public void setFontLostFocus(Font fontLostFocus) {
        this.fontLostFocus = fontLostFocus;
    }
}
