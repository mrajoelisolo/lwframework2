package lwcanvas.components;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import lwcanvas.util.LWGraphicsHelper;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractShape;
import lwcanvas.util.LWFontProvider;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 */
public class LWLabel extends LWAbstractShape implements IShape {
    private String text;
    private Point pos = new Point();
    private Font font = LWFontProvider.BTN_FONT_NORMAL;

    public LWLabel(String text) {
        this.text = text;
    }

    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);
        g2.setColor(penColor);
        g2.setFont(font);
        g2.drawString(text, pos.x, pos.y);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        return false;
    }

    public void setLocation(int x, int y) {
        pos.setLocation(x, y);
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public void append(String seq) {
        this.text += seq;
        System.out.println(""+seq);
    }

    public void setText(String text) {
        this.text = text;
    }
}
