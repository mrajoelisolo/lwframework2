package lwcanvas.components;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import lwcanvas.util.LWGraphicsHelper;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractInteractiveShape;
import lwcanvas.util.LWColorProvider;
import lwcanvas.util.LWFontProvider;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 * @version 1.2
 * @since 2010-03-09
 */
public class LWButton extends LWAbstractInteractiveShape implements IShape {
    private Rectangle bounds = new Rectangle();
    private String caption;

    public LWButton(String caption) {
        this.caption = caption;
    }

    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        LWGraphicsHelper.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);

        if(isFocused()) {
            if(isPressed()) {
                g2.setColor(LWColorProvider.getInstance().getColor(0, 128, 0));
            }
            else {
                g2.setColor(LWColorProvider.getInstance().getColor(0, 64, 0));
            }
            g2.setFont(LWFontProvider.BTN_FONT_COURIER_BOLD_20);
            setPenWidth(2);
        } else {
            g2.setColor(LWColorProvider.getInstance().getColor(0, 16, 0));
            g2.setFont(LWFontProvider.BTN_FONT_COURIER_NORMAL_20);
            setPenWidth(1);
        }

        g2.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);

        g2.setStroke(stroke);
        g2.setColor(penColor);
        g2.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);

        g2.setColor(penColor);
        g2.drawString(caption, bounds.x + 10, bounds.y + bounds.height/2 + 5);

        LWGraphicsHelper.getInstance().restoreGraphics(g2);
    }

    @Override
    public boolean contains(Point p) {
        return bounds.contains(p);
    }

    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }

    public void setLocation(int x, int y) {
        bounds.setLocation(x, y);
    }

    public Point getLocation() {
        return bounds.getLocation();
    }

    public Rectangle getBounds() {
        return bounds;
    }
}
