package lwcanvas.components;

import java.awt.Image;

/**
 *
 * @author Mitanjo
 */
public class LWButtonSkin {
    private Image focusedImg;
    private Image noFocusImg;
    private Image pressedImg;
    private Image disabledImg;

    public LWButtonSkin(Image noFocusImg, Image focusedImg, Image pressedImg, Image disabledImage) {
        this.focusedImg = focusedImg;
        this.noFocusImg = noFocusImg;
        this.pressedImg = pressedImg;
        this.disabledImg = disabledImage;
    }

    public Image getFocusedImg() {
        return focusedImg;
    }

    public void setFocusedImg(Image aFocusedImg) {
        focusedImg = aFocusedImg;
    }

    public Image getNoFocusImg() {
        return noFocusImg;
    }

    public Image getDisabledImg() {
        return disabledImg;
    }

    public void setNoFocusImg(Image aNoFocusImg) {
        noFocusImg = aNoFocusImg;
    }

    public Image getPressedImg() {
        return pressedImg;
    }

    public void setPressedImg(Image aPressedImg) {
        pressedImg = aPressedImg;
    }

    public void setDisabledImg(Image disabledImg) {
        this.disabledImg = disabledImg;
    }

    public boolean isOk() {
        if(focusedImg == null) return false;
        if(noFocusImg == null) return false;
        if(pressedImg == null) return false;
        if(disabledImg == null) return false;

        return true;
    }
}
